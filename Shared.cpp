struct Expression;
struct Number;
struct BinaryOperation;

struct SmartCount
{
    private:
        size_t counter = 0;
    public:
        SmartCount(size_t counter = 0) : counter(counter) {};
        size_t in(size_t in) { return (counter = in); }
        size_t inc() { return (++counter); }
        size_t dec() { return (--counter); }
        bool   zer() { return (counter == 0); }
};

struct SharedPtr
{
    explicit SharedPtr(Expression *ptr = 0) : ptr_(ptr)
    {
        count = new SmartCount(1);
    }
    
    SharedPtr(const SharedPtr & inp) : ptr_(inp.ptr_), count(inp.count)
    {
        count->inc();
    }

    ~SharedPtr()
    {
        if (count->dec() == 0)
        {
            delete ptr_;
            delete count;
            ptr_ = 0;
        }
    }

    SharedPtr& operator=(const SharedPtr & inp)
    {
        if (&inp == this)
            return *this;
        else if (count->dec() == 0)
        {
            delete ptr_;
            delete count;
            ptr_ = 0;
        }
        this->ptr_ = inp.ptr_;
        count = inp.count;
        count->inc();
        return *this;
    }

    void reset(Expression *ptr = 0)
    {
        if (count->dec() == 0)
        {
            delete ptr_;
            delete count;
            ptr_ = 0;
        }
        this->ptr_ = ptr;
        count = new SmartCount(1);
    }
    
    Expression* get() const
    {
        return this->ptr_;
    }
    
    Expression& operator*() const
    {
        return *ptr_;
    }
    
    Expression* operator->() const
    {
        return ptr_;
    }
    
    Expression    *ptr_;
    SmartCount    *count;
};